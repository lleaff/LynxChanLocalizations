Each branch holds material for a language. Instructions on how to use language packs and front-ends can be found at the [LynxChan](https://gitlab.com/mrseth/LynxChan) repository.
